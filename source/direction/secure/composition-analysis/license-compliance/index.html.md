---
layout: markdown_page
title: "Category Direction - License Compliance"
description: "GitLab's goal is to provide License Compliance as part of the standard development process. Learn more!"
canonical_path: "/direction/secure/composition-analysis/license-compliance/"
---

- TOC
{:toc}

## License Compliance

| | |
| --- | --- |
| Stage | [Secure](/direction/secure/) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2023-03-22` |
| Content Last Updated  | `2023-03-22` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

Thanks for visiting this category direction page on License Compliance in GitLab. This page belongs to the [Composition Analysis](/handbook/product/categories/#composition-analysis-group) group of the Secure stage and is hiring a new Product Manager. The interim Product Manager is [Sam White](https://gitlab.com/sam.white).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ALicense%20Compliance) or [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=Category:License+Compliance) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for license compliance, we'd especially love to hear from you.

### Overview
<!-- Describe your category so that someone who is not familiar with the market space can understand what the product does. 
-->

License compliance analyzes the dependencies used in a project.  It identifies dependencies that have been directly included and it also analyzes those dependencies to get a list of their dependencies (also known as indirect or transitive dependencies).  Once a full listing of all direct and transitive dependencies has been obtained, license compliance solutions analyze those dependencies to identify how those dependencies are licensed.  Typically this information is stored in the metadata for the package; however, it may also be present in a file in the dependency's code repository.  This type of analysis allows users to get a full list of all the licenses they are using in the project so they can ensure they are willing to adhere to the associated terms and conditions.

License Compliance is often considered an element of Software Composition Analysis, [Software Bill of Materials (SBOM)](https://gitlab.com/groups/gitlab-org/-/epics/858), and compliance activities.

> GitLab was named as a [Challenger in the 2022 Magic Quadrant for Application Security Testing](https://about.gitlab.com/analysts/gartner-ast22/).

Additional details about our current features and capabilities can be viewed in our documentation:

- [License scanning](https://docs.gitlab.com/ee/user/compliance/license_scanning_of_cyclonedx_files/)
- [License approval policies](https://docs.gitlab.com/ee/user/compliance/license_approval_policies.html)
- [License list](https://docs.gitlab.com/ee/user/compliance/license_list.html)

### Strategy and Themes
<!-- Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

We have three main themes in our license compliance strategy:

1. **Shifting license compliance left** - Our goal is to provide License Compliance as part of the standard development process. This means that License Compliance is executed every time a new commit is pushed to a branch, identifying newly introduced licenses in the merge request. We also include License Compliance as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).
1. **Comprehensive license detection** - Our goal is to continue to add data to our license database so we can cover both a high percentage of the total open source packages available for the languages that we support.  Ideally we will achieve full coverage for the most popular packages for each language that we support.
1. **Accurate license detection** - Our goal is to support multiple license detection methods and to allow those methods to work together to identify licenses accurately.  Eventually we would like to add support for full SPDX expressions so we can accurately handle packages with complex license scenarios.

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them. 
 -->

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

In GitLab 15.9 (for SaaS) and 15.10 (for self managed) we released support for a new method of license compliance scanning that no longer requires use of the `Jobs/License-Scanning.gitlab-ci.yml` template. Instead we now rely on the Dependency Scanning job to gather a list of dependencies and then we compare that list to licenses that are stored in our proprietary license database. This change helps users reduce the number of compute credits that they consume by gathering the list of dependencies just once in the Dependency Scanning job. It also gives GitLab greater control over the accuracy and completeness of the licenses that are reported as we now use our own license database.  This new method of license scanning is intended to replace the previous License Scanning job.  The [License Scanning CI template is now deprecated](https://docs.gitlab.com/ee/update/deprecations.html#license-compliance-ci-template) and is planned to be removed in the GitLab 16.0 release.

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

During the GitLab 15.11 milestone, we are working on [improving the functional behavior](https://gitlab.com/groups/gitlab-org/-/epics/9994) and accuracy of our new license scanning method. Our goal is to achieve approximate feature parity (or better) compared with the functionality provided by the previous method of license scanning that is now deprecated.

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

Over the next few months, we plan to continue to improve and [mature our new method of license scanning](https://gitlab.com/groups/gitlab-org/-/epics/9995) to make it a more reliable source of license information.

#### What is Not Planned Right Now

We currently do not have plans to add the following functionality during the next 12 months:
1. Full support for advanced SPDX expressions
1. Automatic categorization of licenses by type

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecasted near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

#### Key Capabilities 

For this product area, these are the capabilities a best-in-class solution should provide:

1. The ability to accurately and comprehensively detect licenses for both direct and transitive dependencies in a project.
1. Support for detecting licenses across a wide variety of programming languages.
1. Support for multiple methods of detecting licenses (metadata, files, embedded licenses).
1. The ability to detect licenses for closed-source dependencies.
1. The ability to support both basic and complex SPDX expressions.
1. The ability to easily show legal and compliance teams the terms and conditions of licenses that are being included.
1. The ability to enforce policies prohibiting specific licenses from being used in an organization.
1. Reporting and filtering to allow users to easily see which licenses are already in use.
1. Automatic license classification to organize licenses into categories based on their terms and conditions.

This list represents some key capabilities of a comprehensive license compliance solution.  Not all of these capabilities are currently supported by GitLab today.

#### Roadmap
<!-- Key deliverables we're focusing on to build a BIC solution. List the epics by title and link to the epic in GitLab. Minimize additional description here so that the epics can remain the SSOT. This may be duplicative to the 1 year section however for some categories the key deliverables required to become the BIC solution will extend beyond one year and we want to capture all of the gaps. Moreover, the 1 year section may contain work that is not directly related to closing gaps if we are already the BIC or if we are differentiating ourselves.-->

Our [prioritized roadmap](https://about.gitlab.com/direction/secure/composition-analysis/#priorities) can be viewed on our group direction page.  Plans to move the category from [Minimal to Viable](https://gitlab.com/groups/gitlab-org/-/epics/1662) and from [Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/9886) are tracked in GitLab.

#### Top Competitive Solutions
<!-- PMs can choose to highlight a primary BIC competitor--or more, if no single clear winner in the category exists; in this section we should indicate: 1. name of competitive product, 2. links to marketing website and documentation, 3. why we view them as the primary BIC competitor -->

- [Black Duck](https://www.blackducksoftware.com/solutions/open-source-license-compliance)
- [Sonatype Nexus](https://www.sonatype.com/nexus-auditor)
- [Whitesource](https://www.whitesourcesoftware.com/open-source-license-compliance/)
- [snyk](https://snyk.io/product/open-source-license-compliance/)

### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

Primary: [Sasha (Software Developer)](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer) wants to know when adding a dependency if it has licenses that are not permitted per the organization's policy.

Secondary: [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/product/personas/#cameron-compliance-manager) wants to ensure that the organization's license compliance policies are enforced throughout the organization.

### Pricing and Packaging

The GitLab License Compliance features are all packaged as part of the [GitLab Ultimate tier](https://about.gitlab.com/pricing/ultimate/). This aligns with our [pricing strategy](https://about.gitlab.com/company/pricing/#pricing-strategy) as these features are relevant for executives who are concerned about keeping their organization compliant with license terms and conditions.

### Analyst Landscape

License Compliance is frequently bundled together with Container Scanning and Dependency Scanning to provide an overall Software Composition Analysis (SCA) solution within the Application Security Testing (AST) market.  GitLab was recently named as a [Challenger in the 2022 Magic Quadrant for Application Security Testing](https://about.gitlab.com/analysts/gartner-ast22/).
