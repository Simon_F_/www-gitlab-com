<!--
Fill in the following placeholders:
  - {MVP_WINNER} name
  - {X.Y} release version (4 placeholders to be replaced)
  - {NOMINATION_ISSUE} link
  - {NOMINATOR} user handle
  - {COMMENTER} user handle
  - {MONTH} of release
 -->
Hi **{MVP_WINNER}** :wave: 

Congrats on winning GitLab's **{X.Y}** MVP!

We are working on a write-up for you that will be included in the **{X.Y}** release post. For reference you can check out our past [MVPs list](https://about.gitlab.com/community/mvp/) and here are a few notable examples:
- https://about.gitlab.com/releases/2023/02/22/gitlab-15-9-released/#mvp
- https://about.gitlab.com/releases/2022/10/22/gitlab-15-5-released/#mvp
- https://about.gitlab.com/releases/2022/03/22/gitlab-14-9-released/#mvp
- https://about.gitlab.com/releases/2022/02/22/gitlab-14-8-released/#mvp 
- https://about.gitlab.com/releases/2021/05/22/gitlab-13-12-released/#mvp

Please let us know if there are any details you would like us to highlight about yourself, your work or your contributions to the GitLab community. You can also share thoughts about the specific contributions you made for the **{X.Y}** release.

I'm also pinging **{NOMINATOR}** **{COMMENTER}** who either nominated or commented on your contributions in the **{NOMINATION_ISSUE}**. They can also chime in with anything worth noting for the release post write-up or a quote about your contributions.

Our deadline for submitting this write-up is the 20th of **{MONTH}** so we only have a few days to put this together. If we don't hear back or you don't have the time we will do our best to put something together! The **{X.Y}** release post will go live on the 22nd.

Finally we will work with the community relations team to get your GitLab swag sent over!

## Working Draft for Write-Up
<!-- Include any draft updates here in the top-level description -->
