---
layout: markdown_page
title: "Talent Acquisition Alignment"
description: "This page is an overview of the search team alignment and the talent acquisition platform directly responsible individual in talent acquisition operations and talent brand."
---

## Search Team Alignment by Department

| Department                    | Recruiter   | Candidate Experience Specialist    |
|--------------------------|-----------------|-------------------------------------|
| Executive          | Rich Kahn    | Sruthy Menon  |
| Executive          | Zach Choquette   | Sruthy Menon  |
| Enterprise Sales, NA | Kevin Rodrigues |Fernando Khubeir |
| Enterprise Sales, NA | TBD |Mathea Gervolino |
| Enterprise Sales, NA | TBD | Lerato Thipe |
| Commercial Sales,	AMER | Marcus Carter | Fernando Khubeir |
| Commercial Sales,	AMER | Hannah Stewart  | Fernando Khubeir |
| Commercial Sales,	EMEA | Ben Cowdry | Lerato Thipe |
| Channel Sales, US/EMEA | Kanwal Matharu  | Lerato Thipe |
| Field Operations,	US/EMEA | Kelsey Hart  | Fernando Khubeir |
| Customer Success, EMEA | Joanna Muttiah & Ornella Gerca | Lerato Thipe |
| Customer Success, NA | Barbara Dinoff |  Sruthy Menon |
| All Sales, APAC | Yas Priatna  | Lerato Thipe |
| Marketing, Global | Steph Sarff | Michelle Jubrey |
| Marketing, BDR/SDR (Global)| Caroline Rebello |  Michelle Jubrey |
| G&A, Legal | Steph Sarff | Fernando Khubeir  |
| G&A, Accounting, People | Steph Sarff | Fernando Khubeir |
| G&A, IT | Kelsey Hart | Fernando Khubeir |
| Internal | Jenna VanZutphen | Sruthy Menon |
| Development | Mark Deubel & Sara Currie | Guido Rolli |
| Quality | Riley Smith & Dielle Kuffel |  Guido Rolli |
| Infrastructure   | Riley Smith & Dielle Kuffel | Michelle Jubrey |
| Support | Joanna Michniewicz  |  Mathea Gervolino |
| Security | Matt Angell | Michelle Jubrey|
| Incubation | Matt Angell  | Mathea Gervolino |
| Product Management  | Matt Angell  | Mathea Gervolino |
| UX  | Matt Angell  | Guido Rolli |

For urgent requests of the Candidate Experience Specialist team, we encourage you to contact them by also tagging @CES in Slack messages and CC'ing CES@gitlab.com on emails.


## Talent Acquisition Leader Alignment

| Department                    | Leader      | 
|--------------------------|-----------------|
| Talent Acquisition         | Jess Dallmar | 
| Talent Brand and Enablement | Devin Rogozinski |
| Talent Acquisition (Sales) | Jake Foster|
| Talent Acquisition (EMEA Sales) | Debbie Harris/Jake Foster |
| Talent Acquisition (Marketing) | Steph Sarff/Jake Foster |
| Talent Acquisition (G&A) | Jake Foster |
| Talent Acquisition (R&D) | Ursela Knezevic |
| Talent Acquisition (R&D: Customer Support & Development) | Paul Hardy/Ursela Knezevic |
| Talent Acquisition (R&D: Infrastructure/Quality, Security, Product/UX, Incubation) | Ursela Knezevic |
| Talent Acquisition (Executive) | Rich Kahn |
| Enablement | Marissa Ferber |
| Candidate Experience | Ale Ayala/Marissa Ferber |

## Talent Acquisition Platform Directly Responsible Individual

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Comparably | Admin  | Devin Rogozinski/Marissa Ferber |
| Comparably | Content Management | Devin Rogozinski |
| Glassdoor | Admin  | Devin Rogozinski |
| Glassdoor | Responding to Reviews  | Devin Rogozinski |
| Glassdoor | Content Management | Devin Rogozinski |
| LinkedIn | Admin - Recruiter  | Devin Rogozinski |
| LinkedIn | Seats | Devin Rogozinski |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Talent Acquisition | Devin Rogozinski |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Devin Rogozinski |
| New Platform(s) | Requests | @domain |
| Recruitment Marketing  | Requests | @domain |
