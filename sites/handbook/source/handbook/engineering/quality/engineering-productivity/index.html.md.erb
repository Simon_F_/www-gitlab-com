---
layout: handbook-page-toc
title: "Engineering Productivity team"
description: "The Engineering Productivity team increases productivity of GitLab team members and contributors by shortening feedback loops and improving workflow efficiency for GitLab projects."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

Peak GitLab customer results, efficiently and sustainably delivered by an unencumbered, and toil-free engineering team.

## Mission

Increase GitLab's customer outcomes through efficiency improvements for our entire engineering team. Develop a comprehensive measure of key outcomes and focus on improvements. Measure quality of life, efficiency, and toil reduction improvements in the context of subjective and objective measures. The Engineering Productivity team can't drive efficiency without being efficient ourselves. Measure what matters and focus on that. Build partnerships across organizational boundaries to deliver broad, multi-functional improvements.

## KPIs

- [Master Pipeline Stability](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#master-pipeline-stability)
- [Review app deployment success rate](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#review-app-deployment-success-rate)
- [Time to First Failure (TtFF)](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#time-to-first-failure)

## OKRs

See [FY24Q1 Engineering Productivity OKRs (internal link)](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/122796026)

- [Obj: ARR - Improve Development Efficiency (internal link)](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/905?iid_path=true)
  - KR: Improve master stability from 94% to 97%
  - KR: Implement dynamic test parallelization
  - KR: Improve test intelligence accuracy
- [Obj: Product - Mature development tools (internal link)](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/930?iid_path=true)
  - KR: Deliver the first iteration of Development Satisfaction Score
  - KR: Mature JH Validation Pipeline
  - KR: GDK Performance Enhancements
- [Obj: Team Growth and Development (internal link)](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1004?iid_path=true)

## Team

### Members

<%= direct_team(manager_role: 'Director of Engineering Productivity', extra_slugs: ['remy-c']) %>

### Stable Counterpart

| Person | Role |
| --- | --- |
| Greg Alfaro | [GDK Project Stable Counterpart](https://about.gitlab.com/handbook/engineering/quality/engineering-productivity/gdk/), Application Security |

### Structure

```mermaid
graph TD
    A[Infrastructure Quality Department]
    A --> B(Engineering Analytics)
    A --> P(Platforms)
    A --> D(Quality Engineering)
    A --> E(Engineering Productivity)
    A --> R(Reliability)

    click A "/handbook/engineering/quality"
    click B "/handbook/engineering/quality/engineering-analytics"
    click D "/handbook/engineering/quality/quality-engineering"
    click E "/handbook/engineering/quality/engineering-productivity"
    click P "/handbook/engineering/infrastructure/platforms/"
    click R "/handbook/engineering/infrastructure/reliability/"

    style E fill:#bbf,stroke:#f66,stroke-width:2px, color:#fff
```
### Communication

| **GitLab Team Handle** | [`@gl-quality/eng-prod`](https://gitlab.com/gl-quality/eng-prod) |
| **Slack Channel** | [`#g_engineering_productivity`](https://gitlab.slack.com/archives/CMA7DQJRX) |
| **Team Boards** | [Team Board](https://gitlab.com/groups/gitlab-org/-/boards/978615) & [Priority Board](https://gitlab.com/groups/gitlab-org/-/boards/1333450) |
| **Issue Tracker** | [`gitlab-org/quality/engineering-productivity/team`](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/) |

### Child Pages
{:.no_toc}

#### [GDK](/handbook/engineering/quality/engineering-productivity/gdk/)
{:.no_toc}

#### [Flaky tests](/handbook/engineering/quality/engineering-productivity/flaky-tests/)
{:.no_toc}

##### [Workflow Automation](/handbook/engineering/quality/engineering-productivity/workflow-automation/)
{:.no_toc}

#### [Make review apps faster](/handbook/engineering/quality/engineering-productivity/make-review-apps-faster/)
{:.no_toc}

### Office hours

Engineering productivity has monthly office hours on the 3rd Wednesday of the month at 3:00 UTC (20:00 PST) on even months (e.g February, April, etc) open for anyone to add topics or questions to the [agenda](https://docs.google.com/document/d/1_4BapuLIufA_OJ1YSdqdq-aLw4kjBLWQZw2hqSKDsJc/edit#). Office hours can be found in the [GitLab Team Meetings calendar](https://about.gitlab.com/handbook/tools-and-tips/#gitlab-team-meetings-calendar)

### Workstream objectives

The Engineering Productivity team focuses on the following workstreams and the associated Epics with workstream specific vision and objectives.

| Tracking Label | Epics |
| --- | --- |
| ~"ep::pipeline" | [GitLab Project Pipeline Improvement](https://gitlab.com/groups/gitlab-org/-/epics/1853) |
| ~"ep::review-apps" | [Improve Review Apps reliability & efficiency](https://gitlab.com/groups/gitlab-org/-/epics/605) |
| ~"ep::triage" | [Quality: Triage](https://gitlab.com/groups/gitlab-org/-/epics/1461) |
| ~"ep::workflow" | [Reviewer Roulette Improvements](https://gitlab.com/groups/gitlab-org/-/epics/3287) |

### Areas of Responsibility

* **See it and find it**: Build automated measurements and dashboards to gain insights into the productivity of the Engineering organization to identify opportunities for improvement.
  * Implement new measurements to provide visibility into improvement opportunities.
  * Collaborate with other Engineering teams to provide visualizations for measurement objectives.
  * Improve existing performance indicators.
* **Do it for internal team**: Increase contributor and developer productivity by making measurement-driven improvements to the development tools / workflow / processes, then monitor the results, and iterate.
  * Identify and implement quantifiable improvement opportunities with proposals and hypothesis for metric improvements.
  * Automated [merge request quality checks](https://docs.gitlab.com/ee/development/dangerbot.html) and [code quality checks](https://docs.gitlab.com/ee/development/contributing/style_guides.html).
  * [GitLab project pipeline](https://docs.gitlab.com/ee/development/pipelines.html) improvements to improve efficiency, quality or duration.
* **Dogfood use**: Dogfood GitLab product features to improve developer workflow and provide feedback to product teams.
  * Use new features from related product groups (Analytics, Monitor, Testing).
  * Improve usage of [Review apps](https://docs.gitlab.com/ee/development/testing_guide/review_apps.html) for GitLab development and testing.
* **Engineering support**: Participate in activities related to [Engineering MR Rate](/handbook/engineering/performance-indicators/#engineering-mr-rate) and [Quality KPIs](/handbook/engineering/quality/performance-indicators/).
  * [`#master-broken`](/handbook/engineering/workflow/#broken-master) pipeline monitoring.
  * KPI corrective actions such as [Review Apps stabilization](/handbook/engineering/quality/performance-indicators/#review-app-deployment-success-rate-for-gitlab).
  * [Merge Request Coach](/job-families/expert/merge-request-coach/) for ~"Community contribution" merge requests.
* **Engineering workflow**: Develop automated processes for improving label classification hygiene in support of product and Engineering workflows.
  * [Automated issues and merge requests triage](/handbook/engineering/quality/triage-operations/).
  * Improvements to the labelling classification and automation used to support Engineering measurements.
  * See the [`gitlab-triage` Ruby gem](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage), and [Triage operations](https://gitlab.com/gitlab-org/quality/triage-ops) projects for examples.
* **Do it for wider community**: Increase efficiency for wider GitLab Community contributions.
* **Dogfood build**: Enhance and add new features to the GitLab product to improve engineer productivity.

### Meetings

Engineering Productivity has an alternating [weekly team meeting schedule](https://docs.google.com/document/d/1yzHuosvykzb_kaldjGP9I6wxpNpMXdNEvEFBkHyZpXs/edit#) to allow for all team members to collaborate in times that work for them.

- Week 1 is Tuesdays 1200 UTC, 0400 PST
- Week 2 is Tuesdays 2330 UTC, 1530 PST

### Showcase

Showcases are done every two months and will be voted on by the team asynchronously in an issue.

1. Engineering Manager will create an issue to discuss showcase during the second week of even months.
1. Team members will identify topics they'd like to learn more about or demo for the team.
1. Team members vote with `:thumbsup:` reactions on the ideas they'd like to hear about.
1. Engineering Manager will identify a DRI to lead the showcase and schedule for the second week of the odd month.

### Work prioritization

The Engineering Productivity team uses [modified prioritization and planning guidelines](prioritization.html) for targeting work within a Milestone.

### Projects

1. `gitlab-org/gitlab` [pipeline configuration, optimization, and stability](https://docs.gitlab.com/ee/development/pipelines.html).
1. `gitlab-org/gitlab` [Review apps](https://docs.gitlab.com/ee/development/testing_guide/review_apps.html) provisioning and [infrastructure](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/tree/main/review-apps).
1. [Triage operations](https://gitlab.com/gitlab-org/quality/triage-ops) for issues/merge requests/epics, and [infrastructure](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/tree/main/qa-resources).
1. [`gitlab-triage` Ruby gem](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage), used by [Triage operations](https://gitlab.com/gitlab-org/quality/triage-ops).
1. [Development department metrics](/handbook/engineering/development/performance-indicators) for measurements of Quality and Productivity.
1. [`gitLab-dangerfiles` Ruby gem](https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles) for shared [Danger](https://docs.gitlab.com/ee/development/dangerbot.html#danger-bot) rules and plugins.
1. [`gitLab-styles` Ruby gem](https://gitlab.com/gitlab-org/ruby/gems/gitlab-styles) for shared [RuboCop cops](https://docs.gitlab.com/ee/development/contributing/style_guides.html#ruby-rails-rspec).
1. [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit) continued development.
1. [GitLab feature flag alert](https://gitlab.com/gitlab-org/gitlab-feature-flag-alert) for reporting on GitLab feature flags.
1. [GitLab RSpec Profiling Statistics](https://gitlab.com/gitlab-org/rspec_profiling_stats) for profiling information on RSpec tests in CI.

### Metrics

The Engineering Productivity team creates metrics in the following sources to aid in operational reporting.

- [Engineering Productivity Dashboard](https://app.periscopedata.com/app/gitlab/1115916/Engineering-Productivity-Dashboard)
- [Quality Department KPIs](https://app.periscopedata.com/app/gitlab/516343/Quality-Department-KPIs)
- [Broken Master Pipeline Root Cause Analysis](https://app.periscopedata.com/app/gitlab/1082465/Master-Broken-Incidents-Root-Cause-Analysis)
- [Time to First Failure](https://app.periscopedata.com/app/gitlab/878780/Time-to-first-failure-(TtFF))
- [Test Intelligence Accuracy](https://app.periscopedata.com/app/gitlab/1116767/Test-Intelligence-Accuracy)
- [Engineering Productivity Pipeline Health](https://app.periscopedata.com/app/gitlab/564156/EP---Pipelines-health)
- [Engineering Productivity Jobs Durations](https://app.periscopedata.com/app/gitlab/652085/Engineering-Productivity---Pipeline-Build-Durations)
- [Engineering Productivity Package And QA Durations](https://app.periscopedata.com/app/gitlab/869271/Engineering-Productivity---Package-And-QA-Durations)
- [Issue Triage Dashboard](https://app.periscopedata.com/app/gitlab/621211/EP---Issue-Triage)
- [Engineering Productivity Sandbox](https://app.periscopedata.com/app/gitlab/496118/Engineering-Productivity-Sandbox)
- [GitLab-Org Native Insights](https://gitlab.com/groups/gitlab-org/-/insights)
- [Review Apps monitoring dashboard](https://app.google.stackdriver.com/dashboards/6798952013815386466?project=gitlab-review-apps)
- Triage Reactive monitoring dashboards
  - [Overview dashboard](https://console.cloud.google.com/monitoring/dashboards/builder/e3e9d8fc-54cd-4a98-b4a3-e81f01d37e26?project=gitlab-qa-resources&dashboardBuilderState=%257B%2522editModeEnabled%2522:false%257D&timeDomain=1w)
  - [Processors dashboard](https://console.cloud.google.com/monitoring/dashboards/builder/3338d66b-649c-4ea9-aec9-14ffba96c25f?project=gitlab-qa-resources&dashboardBuilderState=%257B%2522editModeEnabled%2522:false%257D&timeDomain=1w)

### Communication guidelines

The Engineering Productivity team will make changes which can create notification spikes or new behavior for
GitLab contributors. The team will follow these guidelines in the spirit of [GitLab's Internal Communication Guidelines](/handbook/people-group/employment-branding/people-communications/).

#### Pipeline changes

##### Critical pipeline changes

Pipeline changes that have the potential to have an impact on the GitLab.com infrastructure should follow the [Change Management](/handbook/engineering/infrastructure/change-management) process.

Pipeline changes that meet the following criteria must follow the [Criticality 3](/handbook/engineering/infrastructure/change-management/#criticality-3) process:

- update to the [`cache-repo` job](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/cache-repo.gitlab-ci.yml) job

These kind of changes [led to production issues in the past](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/3013).

##### Non-critical pipeline changes

The team will communicate significant pipeline changes to [`#development`](https://gitlab.slack.com/messages/C02PF508L) in Slack and the Engineering Week in Review.

Pipeline changes that meet the following criteria will be communicated:

- addition, removal, renaming, parallelization of jobs
- changes to the conditions to run jobs
- changes to pipeline DAG structure

Other pipeline changes will be communicated based on the team's discretion.

#### Automated triage policies

Be sure to give a heads-up to `#development`,`#eng-managers`,`#product`, `#ux` Slack channels
and the Engineering week in review when an automation is expected to triage more
than 50 notifications or change policies that a large stakeholder group use (e.g. team-triage report).

### Asynchronous Issue Updates

Communicating progress is important but status doesn't belong in one on ones as it can be more appropriately communicated with a broader audience using other methods. The "standup" model used by a lot of organizations practicing scrum assumes a certain time of day for those to happen. In the context of a timezone distributed team, there is no "9am" that the team shares. Additionally, the act of losing and gaining context after completing work for the day only to gain it again to share a status update is context switching. The intended audience of the standup model assumes that it's just the team but in GitLab's model, that means folks need to be aware of where this is being communicated (slack, issues, other). Since this information isn't available to the intended audience, the information needs to be duplicated which at worst means there's no single source of truth and at a minimum means copy pasting information.

The proposal is to trial using an Asynchronous Issue Update model, similar to [what the Package Group uses](https://about.gitlab.com/handbook/engineering/development/ops/package/#async-issue-updates). This process would replace the existing daily standup update we post in Slack with `Geekbot`. The time period for the trial would be a milestone or two, depending on feedback cycles.

The async daily update communicates the progress and confidence using an issue comment and the milestone health status using the Health Status field in the issue. A daily update may be skipped if there was no progress. Merge requests that do not have a related issue should be updated directly. It's preferable to update the issue rather than the related merge requests, as those do not provide a view of the overall progress. Where there are blockers or you need support, Slack is the preferred space to ask for that. Being blocked or needing support are more urgent than email notifications allow.

When communicating the health status, the options are:
- `on track` - when the issue is progressing as planned
- `needs attention` - when the issue requires attention or intervention to keep it on schedule
- `at risk` - when there is a risk the issue will not be completed according to schedule

The async update comment should include:
- what percentage complete the work is, in other words, how much work is done to put all the required MRs in review
- the confidence of the person that their estimate is correct
- notes on what was done and/or if review has started
- it could be good to specify the relevant dependencies in the update, if there are multiple people working on it

Example:
```
**Status**: 20% complete, 75% confident

Expecting to go into review tomorrow.
```

Include one entry for each associated MR

Example:
```
**Issue status**: 20% complete, 75% confident

Expecting to go into review tomorrow.

**MR statuses**:

- !11111+ - 80% complete, 99% confident - docs update - need to add one more section
- !21212+ - 10% complete, 70% confident - api update - database migrations created, working on creating the rest of the functionality next
```

##### How to measure confidence?

Ask yourself, how confident am I that my % of completeness is correct?.

For things like bugs or issues with many unknowns, the confidence can help communicate the level of unknowns. For example, if you start a bug with a lot of unknowns on the first day of the milestone you might have low confidence that you understand what your level of progress is.
Your confidence in the work may go down for whatever reason, it's acceptable to downgrade your confidence. Consideration should be given to retrospecting on why that happened.
#### Weekly Epic updates

A weekly update should be added to epics you're assigned to and/or are actively working on. The update should provide an overview of the progress across the feature. Consider adding an update if epic is blocked, if there are unexpected competing priorities, and even when not in progress, what is the confidence level to deliver by the expected delivery date. A weekly update may then be skipped until the situation changes. Anyone working on issues assigned to an epic can post weekly updates.

The epic updates communicate a high level view of progress and status for quarterly goals using an epic comment. It does not need to have issue or MR level granularity because that is part of each issue updates.

The weekly update comment should include:
- Status: ok, so-so, bad? Is there something blocked in the general effort?
- How much of the total work is done? How much is remaining? Do we have an ETA?
- What's your confidence level on the completion percentage?
- What is next?
- Is there something that needs help/support? (tag specific individuals so they know ahead of time)

##### Examples

Some good examples of epic updates that cover the above aspects:
- https://gitlab.com/groups/gitlab-org/-/epics/8628#note_1090732793
- https://gitlab.com/groups/gitlab-org/-/epics/5152#note_1029337901


## Test Intelligence

As the owner of [pipeline configuration](https://docs.gitlab.com/ee/development/pipelines/index.html) for the [GitLab project](https://gitlab.com/gitlab-org/gitlab), the Engineering Productivity team has adopted several test intelligence strategies aimed to improve pipeline efficiency with the following benefits:
- Shortened feedback loop by prioritizing tests that are most likely to fail
- Faster pipelines to scale better when Merge Train is enabled

These strategies include:
- Predictive test jobs via test mapping
- Fail-fast job
- Re-run previously failed tests early
- Selective jobs via pipeline rules
- Selective jobs via labels

#### Predictive test jobs via test mapping

Tests that provide coverage to the code changes in each merge request are most likely to fail. As a result, merge request pipelines for the [GitLab project](https://gitlab.com/gitlab-org/gitlab) run only the predictive set of tests by default. These include:
- [RSpec predictive jobs](https://docs.gitlab.com/ee/development/pipelines/#rspec-predictive-jobs) which runs relevant RSpec tests that are mapped to the code changes
- [Jest predictive jobs](https://docs.gitlab.com/ee/development/pipelines/#jest-predictive-jobs) which runs relevant Jest tests that are mapped to the code changes

Test mapping is done via the [test_file_finder](https://gitlab.com/gitlab-org/ci-cd/test_file_finder) gem. We supplement the gem with a [static mapping file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/tests.yml) to account for known gaps, as the automated mapping is not always perfect. Additionally, we have introduced several more advanced mappings in [the `detect-tests` CI job](https://gitlab.com/gitlab-org/gitlab/-/blob/03f6d482ab702c760639c04b59f0ea4b55c9e880/.gitlab/ci/setup.gitlab-ci.yml#L101-141) that are not covered by `test_file_finder`. Below are a few examples:

- Run certain system specs if a Javascript file was changed in an MR ([#386754](https://gitlab.com/gitlab-org/gitlab/-/issues/386754))
- Run Javascript tests if certain Rails view files are changed in an MR ([#386719](https://gitlab.com/gitlab-org/gitlab/-/issues/386719))
- Run view specs when Rails partials included in those views are changed in an MR ([#395016](https://gitlab.com/gitlab-org/gitlab/-/issues/395016))

The "Fail-fast" job we are experimenting with is a variation of this strategy. It is to run all of the RSpec tests that are most likely to fail in a single job created in an early stage of the pipeline. See the Fail-fast job section for details.

#### Fail-fast job

There is a [fail-fast job](https://docs.gitlab.com/ee/development/pipelinesx/#fail-fast-job-in-merge-request-pipelines) in each merge request pipeline aimed to run all the RSpec tests that provide coverage for the code changes, hence are most likely to fail. It uses the same [test_file_finder](https://gitlab.com/gitlab-org/ci-cd/test_file_finder) gem for test mapping. The job provides faster feedback by running early and stops the rest of the pipeline right away if any of the fail-fast job tests fail.
Take a look at this [youtube video](https://www.youtube.com/watch?v=FCCbxZky5Nk) for details on how [GitLab](https://gitlab.com/gitlab-org/gitlab) implements the fail-fast job with test_file_finder.
Note that the current design only works with low-impacting merge requests which are only mapped to a small set of tests. If there is a large number of tests that are likely to fail for a merge request, putting them in a single job is not feasible and could result in a long-running bottleneck which defeats its purpose.

Premium GitLab customers, who wish to incorporate the `Fail-Fast job` into their Ruby projects, can set it up with our [Verify/Failfast](https://docs.gitlab.com/ee/ci/testing/fail_fast_testing.html) template.

#### Re-run previously failed tests early

Tests that previously failed in a merge request are likely to fail again, so they provide the most urgent feedback in the next run.
To grant these tests the highest priority, the [GitLab](https://gitlab.com/gitlab-org/gitlab) pipeline [prioritizes previously failed tests by re-running them early](https://docs.gitlab.com/ee/development/pipelines/#re-run-previously-failed-tests-in-merge-request-pipelines) in a dedicated job, so it will be one of the first jobs to fail if attention is needed.

#### Selective jobs via pipeline rules

The GitLab pipeline consists of hundreds of jobs, but not all are necessary for each merge request. For example, a merge request with only changes to documenation files do not need to run any backend tests, so we can exclude all backend test jobs from the pipeline.
See [specify-when-jobs-run-with-rules](https://docs.gitlab.com/ee/ci/jobs/job_control.html#specify-when-jobs-run-with-rules) for how to include/exclude CI jobs based on file changes.
Most of the pipeline rules for the [GitLab project](https://gitlab.com/gitlab-org/gitlab) can be found in <https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/rules.gitlab-ci.yml>.

#### Selective jobs via labels

Developers can add labels to run jobs in addition to the ones selected by the pipeline rules. Those labels start with `pipeline:` and multiple can be applied. A few examples that people commonly use:

- `~"pipeline:run-all-rspec"`
- `~"pipeline:run-all-jest"`
- `~"pipeline:run-as-if-foss"`
- `~"pipeline:run-as-if-jh"`
- `~"pipeline:run-praefect-with-db"`
- `~"pipeline:run-single-db"`

See [docs](https://docs.gitlab.com/ee/development/pipelines/) for when to use these pipeline labels.

## Experiments

This is a list of Engineering Productivity experiments where we identify an opportunity, form a hypothesis and experiment to test the hypothesis.

| Experiment | Status | Hypothesis | Feedback Issue or Findings |
| --- | --- | --- | --- |
| [Automatic issue creation for test failures](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/182) | In Progress | The goal is to track each failing test in `master` with an issue, so that we can later automatically quarantine tests. | [Feedback issue](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/182). |
| [Always run predictive jobs for fork pipelines](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/1170) | Complete | The goal is to reduce the compute credits consumed by fork pipelines. The "full" jobs only run for canonical pipelines (i.e. pipelines started by a member of the project) once the MR is approved. | |
| [Retry failed specs in a new process after the initial run](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/1148) | Complete | Given that a lot of flaky tests are unreliable due to previous test which are affecting the global state, retrying only the failing specs in a new RSpec process should result in a better overall success rate. | [Results show that this is useful](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/1148#note_914106156). |
| [Experiment with automatically skipping identified flaky tests](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/1069) | Complete - Reverted | Skipping flaky tests should reduce the number of false broken `master` and increase the `master` success rate. | We found out that it can actually break `master` in some cases, so we reverted the experiment with [`gitlab-org/gitlab!111217`](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111217). |
| [Experiment with running previously failed tests early](https://gitlab.com/gitlab-org/gitlab/-/issues/333857) | Complete | We have not noticed a significant improvement in feedback time due to other factors impacting our Time to First Failure metric. | |
| [Store/retrieve tests metadata in/from pages instead of artifacts](https://gitlab.com/gitlab-org/gitlab/-/issues/335675) | Complete | We're only interested in the latest state of these files, so using Pages makes sense here. This simplifies the logic to retrieve the reports and reduce the load on GitLab.com's infrastructure. | This has been [enabled since 2022-11-09](https://gitlab.com/gitlab-org/gitlab/-/issues/377423#note_1166315874). |
| [Reduce pipeline cost by reducing number of rspec tests before MR approval](https://gitlab.com/gitlab-org/gitlab/-/issues/336063) | Complete | Reduce the CI cost for GitLab pipelines by running the most applicable rspec tests for changes prior to approval | Improvements needed to [identify and resolve selective test gaps](https://gitlab.com/groups/gitlab-org/quality/engineering-productivity/-/epics/6) as this impacted pipeline stability. |
| [Enabling developers to run failed specs locally](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/58569) | Complete | Enabling developers to run failed specs locally will lead to less pipelines per merge request and improved productivity from being able to fix regressions more quickly | [Feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/327660). |
| [Use dynamic analysis to streamline test execution](https://gitlab.com/gitlab-org/gitlab/-/issues/222369) | Complete | Dynamic analysis can reduce the amount of specs that are needed for MR pipelines without causing significant disruption to master stability | [Miss rate of 10%](https://gitlab.com/gitlab-org/gitlab/-/issues/222369#note_480768617) would cause a large impact to master stability. Look to leverage dynamic mapping with local developer tooling. Added [documentation](https://docs.gitlab.com/ee/development/pipelines/index.html#rspec-predictive-jobs) from the experiment. |
| [Using timezone for Reviewer Roulette suggestions](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/34862) | Complete - Reverted | Using timezone in Reviewer Roulette suggestions will lead to a reduction in the mean time to merge | Reviewer Burden was inconsistently applied and specific reviewers were getting too many reviews compared to others. More details in the [experiment issue](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/563#note_397680373) and [feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/227123) |
