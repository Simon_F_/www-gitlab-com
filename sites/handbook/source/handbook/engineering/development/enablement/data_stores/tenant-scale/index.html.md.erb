---
layout: handbook-page-toc
title: Tenant Scale Group
description: "The Tenant Scale Group is the direct outcome of applying our value of Iteration to the direction of the Database Scalability Working Group."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## About

The Tenant Scale group (formerly Pods or Sharding group) is part of the [Data
Stores stage](/handbook/engineering/development/enablement/data_stores/). We
offer support for groups, projects, and user profiles within our product, but
our main focus is a long-term horizontal scaling solution for GitLab.

This page covers processes and information specific to the Tenant Scale group.
See also the [direction page](/direction/enablement/tenant-scale/) and the
[features we support per category](/handbook/product/categories/features/#data-storestenant-scale-group).

### Contact

To get in touch with us, it's best to create an issue in the relevant
project (typically [GitLab](https://gitlab.com/gitlab-org/gitlab)) and add the
`~"group::tenant scale"` label, along with any other appropriate labels.

For urgent items, feel free to use the Slack channel (internal): [#g_tenant-scale](https://gitlab.slack.com/archives/g_tenant-scale).

### Vision

There are multiple proposals and ideas to increase horizontal scalability via
solutions such as database sharding and tenant isolation. The objective of this
group is to explore, iterate on, validate, and lead implementation of proposals
to provide a solution to accommodate GitLab.com's daily-active user growth.

As we brainstorm and iterate on horizontal scalability proposals, we will
provide implementation details, prototypes, metrics, demos, and documentation to
support our hypotheses and outcomes.

### Goals

The executive summary goals for the Tenant Scale group include:

- Support GitLab.com's daily-active user growth
- Do not allow a problem with any given data store to affect all users
- Minimize or eliminate complexity for our self-managed use-case

### Team Members

The following people are permanent members of the Tenant Scale group:

<%= direct_team(manager_slug: 'arturo-herrero') %>

### Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(
  role_regexp: /(Tenant Scale|Principal Engineer, Data Stores|Distinguished Engineer, Ops and Enablement)/,
  direct_manager_role: 'Engineering Manager, Tenant Scale'
) %>

### Team IC Gearing

**Exception Ratio**: 1 Distinguished Engineer, multiple Staff Engineers.

**Justification**: The Tenant Scale group was formed to implement a sharding
solution to solve our database limitations as outlined by the [Database
Scalability Working Group](https://about.gitlab.com/company/team/structure/working-groups/database-scalability/).
The project requires deep knowledge of both PostgreSQL and the application
itself. As the working group has noted, _this problem cannot be solved to meet
our needs and requirements if we limit ourselves to the database: we must
consider careful changes in the application to make it a reality_. Given the
complexity of the task, we have carefully crafted a team with a mix of interests
and expertise to achieve success. We also anticipate the team will continue to
require the expertise of multiple staff+ level engineers to accomplish our
horizontal scalability goals.

## Meetings
Where we can we follow the GitLab values and communicate asynchronously. However, there have a few important recurring meetings.  Please reach out to the [#g_tenant-scale](https://gitlab.slack.com/archives/C01TQ838Y3T) Slack channel if you'd like to be invited.

With the globally distributed nature of this team, it is unlikely that we will have a synchronous meeting where everyone will attend. When we have synchronous meetings we will record the meetings and share written summaries with the links to the recordings. Currently we have the following recurring meetings scheduled.

 - Weekly Monday - Tenant Scale Group Sync (APAC/EMEA) 8:30AM UTC (2:30AM PDT)
 - Weekly Wednesday - Tenant Scale Group Sync (EMEA/AMER) 2:30PM UTC (7:30AM PDT)

## Work
We follow the GitLab [engineering workflow](/handbook/engineering/workflow/) guidelines. To bring an issue to our attention please add the `~"group::pods"` label along with any other relevant labels. If it is an urgent issue, please reach out to the Product Manager or Engineering Manager listed in the [Stable Counterparts](/handbook/engineering/development/enablement/data_stores/tenant-scale/#stable-counterparts) section above.
- Issue Creation Workflow - our issue creation workflow can be viewed [here](./issue-creation-workflow.html)

### Boards

- [Pods: Build](https://gitlab.com/groups/gitlab-org/-/boards/2594854?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Apods) for issues assigned specifically to `~"group::pods"`
  - We are following a model similar to the process described in the [Geo Build Board - A short tour](https://www.youtube.com/watch?v=rZW0ou4u-dw&list=PL05JrBw4t0KoY_6FXXVgj7wPE9ZDS4cOw&index=6)
- [Group::Pods Roadmap](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=group::pods)

### Everyone Can Contribute

Currently the Tenant Scale group is in the early stages of working out what the finished product will be. The task ahead of us is vast and complex. The only way we are going to be able to break this down and iterate on it is to build proofs-of-concept.

Currently we are in the proof-of-concept stage, in most cases we will need to take them to their logical conclusion. We might reach out to other GitLab teams to help us validate some of the assumptions we have had to make to in order to continue this exploration process. We are not afraid to determine that a proof-of-concept should not be continued. We will take what we learn from the process and apply it to the next one so we can iterate on PoC's in an efficient way.

We have [short toes](https://about.gitlab.com/handbook/values/#short-toes) and welcome contributions, we are also very happy to talk about Cells with anyone that is interested. If you have a question, comment or just want generally know what we are up to and how we are going about it feel free to contact us, we'll make time.

### Tenant Scale Group Planning
We are working on building ourselves a rolling four quarters roadmap to enable transparency about what we are working on. Please visit our [planning page](./planning.html) to see how this process works. 

### Working Agreement

There are a few factors influencing some of our working agreements here. First, there is a lot of excitement about the goals and expected outcomes of this team. We are working on a really difficult problem, we have lofty expectations as to what we can deliver and many different potential solutions to investigate. Because of the critical nature of what we are going to deliver we need to be able to consistently and concisely describe our current efforts. Second, we are a newly formed team and we need to be explicit about how we work together to ensure success.

Currently we are asking team members to update their issues with:
- Epic Start Date and Due Date entries - this will allow us to dogfood our [roadmap](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=group::pods) functionality within GitLab
- Add `~pods::active` to issues that we know we will be working on in the near future. This helps us to keep our [Pods: Build](https://gitlab.com/groups/gitlab-org/-/boards/2594854?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Apods) tidy, especially the Open and Closed columns

We can continue to iterate on our working agreement as a team to ensure that we are a successful team and exceed expectations.

### Demos

It is expected that we share progress early and often by creating recorded demos. There will be specific requests for demonstration topics, but unscheduled demonstrations are welcome too. When recording a demo it does not need to be polished or perfect. Guidelines below:

- Keep it simple, it doesn't need to be polished
- For planned demos, open an issue to discuss the topics to be demonstrated with the team. Planned demos would include end of milestone or major functionality
- For ad hoc, spur of the moment or other interesting discoveries that you would like to demo, just create it and announce in Slack
- Demonstrate functionality, even if incomplete and only working locally
- The [Tenant Scale group](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr_xKd1sBqZ5ap_MJnniixO) playlist on GitLab Unfiltered contains all the Tenant Scale group demos and other recordings. As soon as the first Cells videos are uploaded a Cells playlist will be created and this page will be updated with the appropriate link.
- Announce the recording availablity to the #g_tenant-scale slack channel

We should be aiming for sharing demos on a weekly basis.

### Proofs of Concept

There will be many opportunities for us to undertake proofs of concept (POC). When we do, we agree to timebox the POC to 4 weeks and ideally to line up with milestone boundaries. When we need to make exceptions to this agreement we will employ [multimodal communication](/handbook/communication/#multimodal-communication) to ensure all stakeholders are properly apprised. 

### Implementation Plans

In the early phases of our work we will create implementation plans for review. As we create these implementation plans we will list them below.

- [Cells blueprint](https://docs.gitlab.com/ee/architecture/blueprints/pods/index.html)
- [Migrate CI Tables To New Database Plan](./migrate-ci-tables-to-new-database-plan.html)

## How We Interview Candidates

We use Ruby on Rails to build our product backed by a PostgreSQL database. You'll be a great fit for the team if you have strong database performance and scaling experience. Tenant Scale group members have the chance to put their mark on the end product and how it will be implemented as well as being at the forefront of building the solution.

Our goal is to ensure all candidates we hire are in the best position to make fantastic contributions to GitLab, using their technical knowledge and creative expertise in solving problems.

We hold the bar high in hiring quality engineers but also understand the interview process can feel like a long process. Below are the stages for interviewing for Tenant Scale group that balances the need for hiring for success and expediating the process.

We aim to set up and book in all interviews in at the beginning of the interview process so candidates can see and prepare for what lays ahead for them. Feedback from candidates on the process is welcomed and a channel for that feedback will be provided at the beginning.

### Interview Stages.
1. Screening and Hiring Manager Interview
  * If the Screening call is held by the Hiring Manager the two parts will be conducted at the same time.
  * If the Screening call is held by one of our Technical Recruiters the Hiring Manager call will be held separately.
  * We will talk about a candidate's expectations of the role and life at GitLab as well as talking about their technical skills at a high level and behavoural questions.
1. Technical Assessment
  * Candidates are given access to a Merge Request for them to review before the interview. At the interview the candidate will discuss their review and pair with two technical assessment staff on the call and make improvements.
  * There may be further technical discussions to gauge the candidates technical fit in the role and the team.
  * Provides a chance for the candidate to delve into the techincal aspects of the role and GitLab's tech stack.
1. Peer Team Member
  * A behavioural style interview with a Product Manager or peer team member.
1. Skip-level Manager
  * As per the Peer Team Member interview and a further chance for the candidate to demonstrate their fit for the role.
## Dashboards

<%= partial "handbook/engineering/metrics/partials/_cross_functional_dashboard.erb", locals: { filter_value: "Tenant Scale" } %>
