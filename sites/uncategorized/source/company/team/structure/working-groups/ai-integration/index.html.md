---
layout: markdown_page
title: "AI Integration"
description: "The GitLab AI Integration Working Group aims to define, coordinate and ramp up integration of AI capabilities into all product areas"
canonical_path: "/company/team/structure/working-groups/ai-integration/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value                                                                                                                                             |
|:----------------|:--------------------------------------------------------------------------------------------------------------------------------------------------|
| Date Created    | 2022-03-23                                                                                                                                        |
| Target End Date | TBD                                                                                                                                               |
| Slack           | [#wg_ai_integration](https://gitlab.slack.com/archives/C04UL8HMW0L) (only accessible from within the company)                                         |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/19jVbWVYUPW3m7d2SzsXa2zXIAW7pSb2tdQ-AXWzT_DE/edit) (only accessible from within the company) |
| Epic/Issue Label | `wg-ai-integration` |
| Epic search     | [Parent epic](https://gitlab.com/groups/gitlab-org/-/epics/9997), [Epic Search](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=wg-ai-integration)            |
| Issue Board     | [Issue Board link](https://gitlab.com/gitlab-org/gitlab/-/boards/5512012?label_name[]=wg-ai-integration)                                                |
| Overview & Status | See [Exit Criteria](#exit-criteria) below |
| Meeting schedule | Tuesdays at 8am Pacific and Thursdays at 1pm Pacific |

## Goal

The GitLab AI Integration Working Group aims to define, coordinate and ramp up integration of AI capabilities into all product areas

### Overview

[Corresponding Epic](https://gitlab.com/groups/gitlab-org/-/epics/9997)

We want to enable all product teams to be able to use advanced AI capabilities for improving and adding functionality to the product so users can be faster and more productive in their DevSecOps lifecycle. For product teams we want to establish a clear and fast way for going from idea, experiment to production when using AI functionality. Incorporating the services, models and knowhow that the MLOps group has built over time and can provide to the wider team.

The working group will facilitate fast experimentation and prototyping of AI capabilities. We will also advise on what must be considered (and in some cases, get explicit approval on) before moving to production, including legal approval, ethical use of AI, potential necessary changes to terms of service, performance implications, hosting cost implications, infrastructure readiness, security readiness, licensing of 3rd party software/services, appropriate GitLab licensing levels for features, value add in helping users achieve their goals and needs, etc.

More information on the effort and plans can be found in the [internal handbook](https://internal-handbook.gitlab.io/handbook/product/ai-strategy/ai-integration-effort/).

### Goals

This is a list of topics that we want to discuss:

- Classification of AI possibilities and complexities
- Ideation and understanding how experimentation can be done
- [Rapid ML Prototyping](https://docs.google.com/document/d/1y-g4DfxKgBRg7vJCGCIGIKL-XWtajyNQSXYldYWgAt4/edit#heading=h.cax3xpkdgfp)
  - API's and Framework for experimentation
  - Feature Flagged Prototypes
  - Determine how teams without Product Design representation can progress with validation
- Groundwork
  - Base API's for all teams
  - Infrastructure
  - Documentation
  - Understanding of Jobs and user needs to be affected
- Existing ML features
- Road to production for features
  - Different gates

### Exit Criteria

The table below lists all exit criteria for the working group. This is the [top-level epic](https://gitlab.com/groups/gitlab-org/-/epics/9997).

| # | Completed Date | Progress | DRI             | Criteria                                                                                                                                        |
|---|----------------|----------|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| 1 | TBD            | 0%       | ----      | Rapid ML Prototyping setup |

## Roles and Responsibilities

| Working Group Role      | Username        | Person                                                                   | Title                                                           |
| :---------------------- | :-------------- | ------------------------------------------------------------------------ | :-------------------------------------------------------------- |
| Executive Stakeholder   | @hbenson        | [Hillary Benson](https://about.gitlab.com/company/team/#hbenson)       | Senior Director, Product Management - Sec, Data Science & Monitor              |
| Executive Stakeholder   | @timzallmann    | [Tim Zallmann](https://about.gitlab.com/company/team/#timzallmann)       | Senior Director of Engineering, Dev                             |
| Facilitator             | @tmccaslin  | [Taylor McCaslin](https://about.gitlab.com/company/team/#tmccaslin)   | Group Manager, Product - Data Science                    |
| Facilitator             | @wayne  | [Wayne Haber](https://about.gitlab.com/company/team/#wayne)   | Director of Engineering |
| Functional Lead - AI Assisted      | @mray  | [Monmayuri Ray](https://gitlab.com/mray2020)   | Engineering Manager AI Assisted |
| Functional Lead - UX             | @jmandell  | [Justin Mandell](https://about.gitlab.com/company/team/#jmandell)   | Product Design Manager: Analytics, Govern, ModelOps, and Secure |
| Functional Lead - Legal             | @m_taylor  | [Matthew Taylor](https://about.gitlab.com/company/team/#m_taylor)   | Sr. Director of Legal |
| Pricing representative             | @seanhall | [Sean Hall](https://about.gitlab.com/company/team/#seanhall)   | Principal Pricing Manager, Product |
| Product representative             | @mushakov | [Melissa Ushakov](https://about.gitlab.com/company/team/#mushakov)   | Group Manager, Product - Plan |
| Product representative             | @sarahwaldner | [Sarah Waldner](https://about.gitlab.com/company/team/#sarahwaldner)   | Group Manager, Product - Create |
| Product representative             | @joshlambert | [Joshua Lambert](https://about.gitlab.com/company/team/#joshlambert)   | Director of Product, Enablement |
| Product representative             | @tlinz | [Torsten Linz](https://about.gitlab.com/company/team/#tlinz)   | PM, Source Code |
| Development representative             | @johnhope | [John Hope](https://about.gitlab.com/company/team/#johnhope)   | SEM, Plan |
| Development representative             | @andr3 | [André Luís](https://about.gitlab.com/company/team/#andr3)   | FEM: Source Code |
| Development representative             | @cdu1  | [Chun Du](https://about.gitlab.com/company/team/#cdu1)   | Director of Engineering, Enablement |
| Development representative             | @igor.drozdov  | [Igor Drozdov](https://about.gitlab.com/company/team/#igor.drozdov)   | Staff Backend Engineer, Source Code |
| Development representative             | @jeromezng | [Jerome Ng](https://about.gitlab.com/company/team/#jeromezng) | Director of Engineering, Fulfillment |
| Legal representative             | @jbackerman | [Jesse Backerman](https://about.gitlab.com/company/team/#jbackerman)   | Managing Legal Counsel |
| Vulnerability Research Representative | @idawson | [Isaac Dawson](https://about.gitlab.com/company/team/#idawson) | Staff Vulnerability Researcher |
| Vulnerability Research Representative | @dbolkensteyn | [Dinesh Bolkensteyn](https://about.gitlab.com/company/team/#dbolkensteyn) | Sr. Vulnerability Researcher |
| Third Party Security Risk Representative | @tdilbeck | [Ty Dilbeck](https://about.gitlab.com/company/team/#tdilbeck) | Security Risk Manager |
| Governance and Field Security Representative | @jlongo_gitlab | [Joseph Longo](https://about.gitlab.com/company/team/#jlongo_gitlab) | Governance and Field Security Manager |
| Security Automation Representative | @agroleau | [Alexander Groleau](https://about.gitlab.com/company/team/#agroleau) | Senior Security Engineering Manager (Automation) |
| Security Automation Representative | @imand3r | [Ian Anderson](https://about.gitlab.com/company/team/#imand3r) | Staff Security Engineer (Automation) |
| Solutions Architecture Representative / Rapid Prototyping Team Member | @bartzhang | [Bart Zhang](https://about.gitlab.com/company/team/#bartzhang) | Channel Solutions Architect |
| Product Marketing Representative| @laurenaalves | [Laurena Alves](https://about.gitlab.com/company/team/#laurenaalves) | Senior Product Marketing Manager |
