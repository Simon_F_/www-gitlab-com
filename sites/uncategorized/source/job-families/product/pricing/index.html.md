---
layout: job_family_page
title: "Pricing"
description: "The Pricing team owns the definition and execution of pricing strategy at GitLab."
---

The Pricing team owns the definition and execution of pricing strategy at GitLab.  This team is incredibly strategic to GitLab's success, and has the opportunity to make a large positive financial impact through improving Average Selling Price and Average Revenue per User.

## Responsibilities
- Define, track and measure pricing performance, defining actionable decision insights on pricing performance
- Build models to inform the impact of price changes on customer acquisition, usage and churn
- Provide critical thinking to make a huge positive impact on GitLab’s top line growth and bottom line profitability

## Requirements
- Self-motivated and self-managing, with strong organizational skills
- Share our values, and work in accordance with those values
- Ability to thrive in a fully remote organization, being collaborative and supportive
- Comfortable with fast-pace and ambiguity in a start-up environment
- Comfort working in a highly agile, intensely iterative software development process
- Ability to use GitLab

## Levels

### Principal Pricing Manager
The Principal Pricing Manager reports to the [Director of Pricing](/job-families/product/pricing/#sr-director-of-pricing).

#### Principal Pricing Manager Responsibilities
- Drive pricing strategy for key initiatives across the business
- Lead the research and analysis efforts to define our pricing model, including pricing metrics, price points, and package construction
- Collaborate effectively with the executive team and product organization to drive complex pricing decisions
- Ensure pricing and packaging policy is effectively implemented into the customer experience
- Implement continuous pricing optimization plans
- Work cross functionally to execute on pricing changes including close collaboration with Product, Sales, Marketing, Finance and Legal departments. 

#### Principal Pricing Manager Requirements
- 7+ years of pricing experience.  MBA preferred.  
- Fluency in pricing research methodologies like conjoint and Van Westendorp Analysis
- Deep analytical skill and experience creating complex models to forecast the impact of pricing changes
- Expertise in quantitative and qualitative customer validation techniques, including surveys & customer interviewing to test customer segmentation, value metrics, pricing models, etc.
- Demonstrated ability to drive complex cross-functional pricing & packaging decisions
- Previous experience using customer/market segmentation techniques to effectively map pricing packages to user personas, buyer personas, and/or market segments

#### Principal Pricing Manager Job Grade

The Principal Pricing Manager is a [grade 9.5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Principal Pricing Manager Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. 

- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters
- Next, candidates will be invited to schedule an interview with the Director of Pricing
- Next, candidates will be invited to schedule an interview with a leader from the Sales Organization (VP of Sales Ops, VP of Sales or CRO)
- Next, candidates will be invited to schedule an interview with a leader from the Marketing Organization (Sr. Director of Strategic Marketing or CMO)
- Next, candidates will be invited to schedule an interview with the VP of Product

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

### Director of Pricing
The Director of Pricing reports to the [Sr Director of Product Monetization](/job-families/product/product-management-leadership/#senior-director-of-product-monetization)

#### Director of Pricing Responsibilities

- Lead the pricing function at the company
- Work closely with the CEO, CMO, CRO, and VP of Product to drive decision making around all aspects of the [pricing model](https://about.gitlab.com/company/pricing/)
- Collaborate effectively cross-functionally to drive complex pricing decisions at all levels including e-group
- Ensure pricing and packaging projects are effectively executed, including customer messaging, product experience, and internal enablement 
- Drive robust pricing related research and analysis to generate and test hypotheses to optimize GitLab’s pricing model, including pricing metrics, price points, and package construction
- Implement continuous pricing optimization plans
- Work across Fulfillment, Sales, Marketing, Legal, and Finance to deliver pricing and packaging changes
- Contribute to our broader strategy collaborating closely with the Product Leadership Team

#### Director of Pricing Requirements

- 15+ years of overall experience, with 8+ years of pricing experience, preferably as a people manager of a pricing team. Previous experience in Product Management also a plus.
- Experience with open source business models a plus
- Experience with both SaaS and self-managed deployment models a plus
- Demonstrated ability to drive complex cross-functional pricing & packaging decisions, as well as subsequent implementation and success tracking
- Fluency in pricing research methodologies like conjoint and Van Westendorp Analysis
- Deep analytical skill and experience creating complex models to forecast the impact of pricing changes
- Expertise in quantitative and qualitative customer validation techniques, including surveys & customer interviewing to test customer segmentation, value metrics, pricing models, etc.
- Previous experience using customer/market segmentation techniques to effectively map pricing packages to user personas, buyer personas, and/or market segments

#### Director of Pricing Job Grade

The Director of Pricing is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director of Pricing Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters
- Next, candidates will be invited to schedule an interview with the Sr Director, Product Monetization
- Next, candidates will be invited to schedule a deep-dive interview with a Principal Pricing Manager
- Next, candidates will be invited to schedule an interview with the Director of Product, Fulfillment
- Next, candidates will be invited to schedule an interview with a leader from the Sales Organization (VP of Field Ops)
- Next, candidates will be invited to schedule an interview with a leader from the Marketing Organization (VP, Product Marketing)
- Finally, candidates may be invited to schedule an interview with the VP of Product Management

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

## Performance Indicators

- Average Revenue per User
- Average Selling Price

## Career Ladder

The next step for the Pricing job family is not yet defined at GitLab. 
